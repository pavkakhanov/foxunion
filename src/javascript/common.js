'use strict'
window.$ = window.jQuery = require('jquery')
require('./vendor/jquery.fullpage.min')
const ytPlayer = require('yt-player')

$(window).on( 'load', function() {
    setTimeout(function() {
        $('body,html').animate({scrollTop: 0}, 1);
    });
});

$(document).ready(function () {

     $('#fullpage').fullpage({
        navigation: true,
        navigationPosition: 'right',
        scrollBar: true
    });

     window.sr = new ScrollReveal()

//Scroll animations
    sr.reveal('.achievments_heading h1, .portfolio_heading h1, .advantages_heading h1, .price_list_heading h1, .our_clients_heading h1',{
        reset:  true,
        duration: 500,
        opacity: 0,
        origin: 'left',
        scale: 1,
        distance: '50px',
        easing : 'ease-out'
    })
    sr.reveal('.achievments_heading p, .portfolio_heading p, .advantages_heading p, .price_list_heading p, .our_clients_heading p',{
        reset:  true,
        duration: 500,
        delay: 300,
        opacity: 0,
        origin: 'left',
        scale: 1,
        distance: '50px',
        easing : 'ease-out'
    })
    sr.reveal('.item_inner_text, .prices_pack, .our_clients_list, .advantages_list, .achievments_list_local',{
        reset:  false,
        duration: 400,
        delay: 300,
        origin: 'top',
        scale: 1,
        distance: '40px',
        easing : 'ease-out'
    })
    sr.reveal('.fullpage_back>p',{
        reset:  true,
        duration: 400,
        delay: 200,
        origin: 'left',
        scale: 1,
        distance: '80px',
        easing : 'ease-out'
    })
    sr.reveal('.map_form',{
        reset:  true,
        duration: 400,
        delay:400,
        origin: 'right',
        scale: 1,
        distance: '80px',
        easing : 'ease-out'
    })
    sr.reveal('.footer_list>li',{
        reset:  true,
        duration: 300,
        delay:100,
        origin: 'top',
        scale: 1,
        distance: '20px',
        easing : 'ease-out'
    })

//Hamburger button
    var Menu = {
        el: {
            ham: $('.navbar_hamburger-button'),
            menuTop: $('.menu-top'),
            menuMiddle: $('.menu-middle'),
            menuBottom: $('.menu-bottom'),
            fullscreenMenu: $('.site_fullscreen_menu'),
            nav: $('.menu_nav'),
            form: $('.callback_form')
        },
        init: function () {
            Menu.bindUIactions();
        },
        bindUIactions: function () {
            Menu.el.ham.on('click', function (event) {
                Menu.activateMenu(event);
                event.preventDefault();
            });
        },
        activateMenu: function () {
            Menu.el.menuTop.toggleClass('menu-top-click');
            Menu.el.menuMiddle.toggleClass('menu-middle-click');
            Menu.el.menuBottom.toggleClass('menu-bottom-click');
            Menu.el.fullscreenMenu.toggleClass('menu_visible');
            Menu.el.nav.toggleClass('site_fullscreen_menu__visible');
        }
    };
    Menu.init();

//Glithy fox on 1-st slide
    $('.glitch_text_1, .glitch_text_2').on('mouseover', function () {
        $('.fox_vid').css('display', 'none')
        $('.fox_vid_glitched').css('display', 'block')

    })
    $('.glitch_text_1, .glitch_text_2').on('mouseout', function () {
        $('.fox_vid').css('display', 'block')
        $('.fox_vid_glitched').css('display', 'none')

    })

//YT player on 2-nd slide

    const player = new ytPlayer('#player',{
        controls:false,
        related: false,
        width:1920,
        height:1080
    });

    player.load('cecPOKlJALs')
    player.setVolume(100)

    $('#play_button, .showreel').click(()=>{
        $('#player').css('top', '0');
        $('.video_close-button').css('top','25px');
        $('.video_close-button>.menu-top').addClass('menu-top-click')
        $('.video_close-button>.menu-bottom').addClass('menu-bottom-click')
        player.play();
    })
    $('.video_close-button').on('click',function () {
        player.stop();
        $('.video_close-button').css('top','-45px')
        setTimeout(function () {
            $('#player').css('top','120vh')
        }, 300)

    })

    player.on('paused', ()=>{
        player.pause();
    })

    player.on('ended',()=>{
        $("#page2_video, .video_overlay, .navbar").css('visibility', 'visible');
        $('#player').css('top','120vh');
    });


//Achievments tabs
    $('label[for="local"]').on('click', ()=> {
        $('.achievments_list_local').css('display', 'grid')
        $('.achievments_list_country').css('display', 'none')
    })
    $('label[for="country"]').on('click', ()=> {
        $('.achievments_list_local').css('display', 'none')
        $('.achievments_list_country').css('display', 'grid')
    })



//3rd slide hover effect
    var someDiv = $('.services_item');
    someDiv.on('mouseover', function() {
        var offset = $(this).offset();
        someDiv.addClass('hovered');
        $(this).removeClass('hovered');
        someDiv.find('.item_video_bg').css('display','none');
        $(this).find('.item_video_bg').css({'display' : 'block', 'opacity':'1', 'left': '-'+offset.left+'px'});
    });
    someDiv.on('mouseout', function() {
        someDiv.removeClass('hovered');
        someDiv.find('.item_video_bg').css('display','none');
    });


//Open map
    $('.map-btn').on('click', function(e) {
        e.preventDefault();
        $('.fullpage_back').css('display', 'none')
        $('#map>.btn').css('display', 'inline-block')
    })
    $('#map>.btn').on('click', function(e) {
        e.preventDefault();
        $('.fullpage_back').css('display', 'grid')
        $(this).css('display', 'none')
    })
});


ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [56.009710,92.814542],
            zoom: 16,
            controls: ['zoomControl'],
            behaviors: ["drag", "dblClickZoom", "rightMouseButtonMagnifier"]
        }, {
            searchControlProvider: 'yandex#search'
        }),
        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: 'Студия Фоксюнион',
            balloonContent: 'Ладо Кецховели 22а, оф. 9-15'
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: './assets/images/marker.png',
            // Размеры метки.
            iconImageSize: [100, 108],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-50,-108]
        });

    myMap.geoObjects.add(myPlacemark);

});
