'use strict'

//Gulp
const   gulp            =       require ('gulp'),

//SASS, JADE
        sass            =       require ('gulp-sass'),
        pug             =       require ('gulp-pug'),
        autoprefixer    =       require ('gulp-autoprefixer'),

//ES6
        browserify      =       require ('browserify'),


//images
        pngquant        =       require ('imagemin-pngquant'),


//Server
        server          =       require ('browser-sync'),
        reload          =       server.reload,


//Filesystem
        watch           =       require ('gulp-watch'),
        source          =       require ('vinyl-source-stream'),
        buffer          =       require ('vinyl-buffer'),


//Minifires
        uglify          =       require ('gulp-uglify'),
        sourcemaps      =       require ('gulp-sourcemaps'),
        minify_css      =       require ('gulp-minify-css'),
        imagemin        =       require ('gulp-imagemin'),


//Misc
        rigger          =       require ('gulp-rigger'),
        uri             =       require ('gulp-data-uri-stream'),
        replace         =       require ('gulp-replace'),
        fileinclude     =       require ('gulp-file-include'),




//Server config
        config = {
    env: process.env.NODE_ENV || 'development',

    path:{
        build:{
            html:   'build',
            js:     'build/assets/js/',
            css:    'build/assets/css',
            images: 'build/assets/images',
            fonts:  'build/assets/fonts',
            svg:    'build/assets/svg/uri'
        },
        src:{
            jade:   'src/*.pug',
            ES:     'src/javascript/common.js',
            scss:   'src/stylesheet/style.scss',
            images: 'src/images/**/*.*',
            fonts:  'src/fonts/**/*.*',
            svg:    'src/svg/*.*'

        },
        watch:{
            html:   'build/*.html',
            jade:   'src/**/*.pug',
            ES:     'src/javascript/common.js',
            scss:   'src/stylesheet/**/*.scss',
            images: 'src/images/**/*.*',
            fonts:  'src/fonts/**/*.*',
            svg:    'src/svg/*.svg'

        },
        clean: 'build',
    },

    server: {
        port: 9000,
        host: 'localhost',
        logPrefix: 'NodeBuilder',
        tunnel: false,
        open: false,
        reloadDelay: 1000,
        reloadDebounce: 1000,
        server:{
            baseDir: "build"
        },
        ui:{
            port:3002
        },
        ghostMode:{
            clicks: false,
            forms: false,
            scroll: false,
        },
        watchOptions:{
            ignoreInitial: true,
            ignore:'.gitkeep'
        }
    }

}

//Tasks
gulp.task('jade::build',()=>{
    return gulp.src(config.path.src.jade)
        .pipe(rigger())
        .pipe(pug({pretty:true})
            .on('error',(e)=>{console.log("OPPA ", e.message)})
            )
        .pipe(gulp.dest(config.path.build.html))
        .pipe(server.stream({once:true}));
})

gulp.task('script::build',()=>{
    return browserify({
     entries:[config.path.src.ES],
     debug: true,
     extensions: ['es6'],
     trasform: ['babelify']
 }).bundle()
     .pipe(source('script.js'))    
     .pipe(buffer())
     .pipe(sourcemaps.init({loadMaps: true,debug: true}))
     .pipe(rigger())
     //.pipe(uglify())
     .pipe(sourcemaps.write('./'))
     .pipe(gulp.dest(config.path.build.js))
     .pipe(server.stream())
     //.pipe(reload({stream: true}));
})

gulp.task('style::build',()=>{
    return gulp.src(config.path.src.scss)
        .pipe(
            sass({includePaths:[

            ],
            imagePath:'/build/assets/images'
            })
        )
        .pipe(fileinclude({prefix: '@@', basepath: './'}))
        .pipe(autoprefixer("last 2 version", "> 1%", "Explorer >= 8", {cascade: true}))
        .pipe(minify_css({compatibility:'ie9'}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(config.path.build.css))
        .pipe(server.stream());
})


gulp.task('images::build',()=>{
    return gulp.src(config.path.src.images)
        .pipe(imagemin({
          progressive: true,
          svgoPlugins: [
            {removeViewbox: false},
            {removeUselessStrokeAndFill: false},
            {removeEmptyAttrs: false}
          ],
          use: [pngquant()],
          interlaced:true
        }))
        .pipe(gulp.dest(config.path.build.images))
        .pipe(server.stream());
})

gulp.task('fonts::build',()=>{
    return gulp.src(config.path.src.fonts)
      .pipe(gulp.dest(config.path.build.fonts));
})

gulp.task('svg::build',()=>{
    return(gulp.src(config.path.src.svg))
      .pipe(uri({encoding:'utf8'}))
      .pipe(replace(/;utf8/g , ''))
      .pipe(gulp.dest(config.path.build.svg));
})

gulp.task('server',()=>{
    server(config.server);
})


gulp.task('live', ()=>{
    gulp.watch([config.path.watch.jade],   ()=> {gulp.start('jade::build')});
    gulp.watch([config.path.watch.scss],   ()=> {gulp.start('style::build')});
    gulp.watch([config.path.watch.ES],     ()=> {gulp.start('script::build')});
    gulp.watch([config.path.watch.images], ()=> {gulp.start('images::build')});
    gulp.watch([config.path.watch.fonts],  ()=> {gulp.start('fonts::build')});
    gulp.watch([config.path.watch.svg],    ()=> {gulp.start('svg::build')});
})

gulp.task('build::all',['svg::build',
                        'jade::build',
                        'script::build',
                        'fonts::build',
                        'images::build',
                        'style::build'],()=>{
})


gulp.task('default',['build::all', 'server', 'live'])